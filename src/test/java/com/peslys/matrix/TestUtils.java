package com.peslys.matrix;

import static org.junit.Assert.assertEquals;

import java.util.Random;

public class TestUtils {

	static final Random RND = new Random();

	private TestUtils() {
	}

	static byte[][] randomArray() {
		return fillRandom(new byte[100 + RND.nextInt(100)][100 + RND.nextInt(100)]);
	}

	static byte[][] fillRandom(byte[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = (byte) (RND.nextBoolean() ? 1 : 0);
			}
		}
		return arr;
	}

	static void assertDataEquals(byte[][] product, final Matrix mx) {
		final int rows = product.length;
		final int columns = product[0].length;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				final int v = product[i][j];
				assertEquals(v, mx.getInt(i + 1, j + 1));
			}
		}
	}

	static void assertDataEquals(final Matrix mx1, final Matrix mx2) {
		final int rows = mx1.getRows();
		assertEquals(rows, mx2.getRows());
		final int columns = mx1.getColumns();
		assertEquals(columns, mx2.getColumns());
		for (int i = 1; i <= rows; i++) {
			for (int j = 1; j <= columns; j++) {
				assertEquals(mx1.getInt(i, j), mx2.getInt(i, j));
			}
		}
	}
}
