package com.peslys.matrix;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import static com.peslys.matrix.TestUtils.RND;
import static com.peslys.matrix.TestUtils.assertDataEquals;

public class BinaryMatrixTest {

	@Test
	public void test_Create_random() {
		testCreate(TestUtils.randomArray());
	}

	private void testCreate(final byte[][] array) {
		final Matrix binMapMatrix = new BitSetBinaryMatrix(array);
		final Matrix arrMatrix = new ArrayBinaryMatrix(array);
		final int rows = array.length;
		final int columns = array[0].length;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				final int v = array[i][j] != 0 ? 1 : 0;
				assertEquals(v, binMapMatrix.getInt(i + 1, j + 1));
				assertEquals(v, arrMatrix.getInt(i + 1, j + 1));
			}
		}
	}

	@Test
	public void test_SetAndGet_random() {
		final int rows = 100 + RND.nextInt(100);
		final int columns = 100 + RND.nextInt(100);
		final AbstractBinaryMatrix<?> binMapMatrix = new BitSetBinaryMatrix(rows, columns);
		final AbstractBinaryMatrix<?> arrMatrix = new ArrayBinaryMatrix(rows, columns);
		for (int i = 1000 + RND.nextInt(1000); i > 0; i--) {
			final int v = (byte) (RND.nextBoolean() ? 1 : 0);
			final int row = 1 + RND.nextInt(rows);
			final int col = 1 + RND.nextInt(columns);
			binMapMatrix.set(row, col, v);
			assertEquals(binMapMatrix.getInt(row, col), v);
			arrMatrix.set(row, col, v);
			assertEquals(arrMatrix.getInt(row, col), v);
		}
	}

	@Test
	public void test_Multiply_1() {
		// @formatter:off
		final byte[][] multiplyer = {
			{1, 1, 0, 1}};
		final byte[][] multiplicand = {
			{1},
			{0},
			{1},
			{1}};
		final byte[][] product = {
			{0}};
		// @formatter:on
		testMultiply(multiplyer, multiplicand, product);
	}

	@Test
	public void test_Multiply_2() {
		// @formatter:off
		final byte[][] multiplyer = {
			{1, 1, 0},
			{0, 1, 1}};
		final byte[][] multiplicand = {
			{1},
			{0},
			{1}};
		final byte[][] product = {
			{1},
			{1}};
		// @formatter:on
		testMultiply(multiplyer, multiplicand, product);
	}

	@Test
	public void test_Multiply_3() {
		// @formatter:off
		final byte[][] multiplyer = {
			{0, 0, 0, 1, 1, 1, 1},
			{0, 1, 1, 0, 0, 1, 1},
			{1, 0, 1, 0, 1, 0, 1}};
		final byte[][] multiplicand = {
			{1},
			{0},
			{1},
			{1},
			{0},
			{1},
			{0}};
		final byte[][] product = {
			{0},
			{0},
			{0}};
		// @formatter:on
		testMultiply(multiplyer, multiplicand, product);
	}

	@Test
	public void test_Multiply_4() {
		// @formatter:off
		final byte[][] multiplyer = {
			{0, 1, 1},
			{1, 1, 0},
			{0, 0, 1}};
		final byte[][] multiplicand = {
			{1, 0, 0},
			{0, 1, 1},
			{1, 0, 1}};
		final byte[][] product = {
			{1, 1, 0},
			{1, 1, 1},
			{1, 0, 1}};
		// @formatter:on
		testMultiply(multiplyer, multiplicand, product);
	}

	private void testMultiply(byte[][] multiplyer, byte[][] multiplicand,
			byte[][] product) {
		{
			final BitSetBinaryMatrix bmx1 = new BitSetBinaryMatrix(multiplyer);
			final BitSetBinaryMatrix bmx2 = new BitSetBinaryMatrix(multiplicand);
			assertDataEquals(product, bmx1.multiplyNaive(bmx2));
			assertDataEquals(product, bmx1.multiply(bmx2));
			assertDataEquals(product, bmx1.multiplyNativeNaive(bmx2));
			assertDataEquals(product, bmx1.multiplyRowsByExecutors(bmx2, 2, 2));
		}
		{
			final ArrayBinaryMatrix amx1 = new ArrayBinaryMatrix(multiplyer);
			final ArrayBinaryMatrix amx2 = new ArrayBinaryMatrix(multiplicand);
			assertDataEquals(product, amx1.multiplyNaive(amx2));
			assertDataEquals(product, amx1.multiply(amx2));
			assertDataEquals(product, amx1.multiplyNativeNaive(amx2));
			assertDataEquals(product, amx1.multiplyRowsByExecutors(amx2, 2, 2));
		}
	}
}
