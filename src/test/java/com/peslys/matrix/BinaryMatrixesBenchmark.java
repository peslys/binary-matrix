package com.peslys.matrix;

import static com.peslys.matrix.TestUtils.assertDataEquals;

public class BinaryMatrixesBenchmark {

	public static void main(String[] args) {
		final int size = 1250;
		final int blockCount = 16;
		final int threadCount = 16;
		long ms;
		System.out.println("Preparing data...");
		final byte[][] dat1 = new byte[size][size];
		TestUtils.fillRandom(dat1);
		final byte[][] dat2 = new byte[size][size];
		TestUtils.fillRandom(dat2);
		Matrix mxr2;

		System.out.println("Preparing matrixes...");
		final ArrayBinaryMatrix ma1 = new ArrayBinaryMatrix(dat1);
		assertDataEquals(dat1, ma1);
		final ArrayBinaryMatrix ma2 = new ArrayBinaryMatrix(dat2);
		assertDataEquals(dat2, ma2);

		System.out.println("Starting benchmark...");
		ms = System.currentTimeMillis();
		final Matrix mxr1 = ma1.multiplyNaive(ma2);
		System.out.println("Array default: " + (System.currentTimeMillis() - ms));

		ms = System.currentTimeMillis();
		mxr2 = ma1.multiplyNativeNaive(ma2);
		System.out.println("Array native: " + (System.currentTimeMillis() - ms));
		assertDataEquals(mxr1, mxr2);

		ms = System.currentTimeMillis();
		ma1.multiplyRowsByExecutors(ma2, blockCount, threadCount);
		System.out.println("Array native rows by block executors: "
				+ (System.currentTimeMillis() - ms));
		assertDataEquals(mxr1, mxr2);

		final BitSetBinaryMatrix mb1 = new BitSetBinaryMatrix(dat1);
		assertDataEquals(dat1, mb1);
		final BitSetBinaryMatrix mb2 = new BitSetBinaryMatrix(dat2);
		assertDataEquals(dat2, mb2);

		ms = System.currentTimeMillis();
		mxr2 = mb1.multiplyNaive(mb2);
		System.out.println("BinMap default: " + (System.currentTimeMillis() - ms));
		assertDataEquals(mxr1, mxr2);

		ms = System.currentTimeMillis();
		mxr2 = mb1.multiplyNativeNaive(mb2);
		System.out.println("BinMap native: " + (System.currentTimeMillis() - ms));
		assertDataEquals(mxr1, mxr2);

		ms = System.currentTimeMillis();
		mxr2 = mb1.multiplyRowsByExecutors(mb2, blockCount, threadCount);
		System.out.println(
				"BinMap native rows by executors: " + (System.currentTimeMillis() - ms));
		assertDataEquals(mxr1, mxr2);
	}
}
