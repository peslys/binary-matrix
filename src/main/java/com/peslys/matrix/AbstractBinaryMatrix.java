package com.peslys.matrix;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

abstract class AbstractBinaryMatrix // NOSONAR (hiding implementation)
<T extends AbstractBinaryMatrix<?>> implements Matrix {

	abstract void set(int row, int col, int value);

	abstract void multiplyNative(T other, T dest, int fromRow, int toRow);

	abstract T create(int rows, int columns);

	final T multiplyRowsByExecutors(final T other, final int blocksCount,
			final int thredsCount) {
		final int thisRows = this.getRows();
		final int blockSize =
				thisRows / blocksCount + (thisRows % blocksCount != 0 ? 1 : 0);
		final T dest = create(thisRows, other.getColumns());
		final ExecutorService service;
		if (thredsCount > 0) {
			service = Executors.newFixedThreadPool(thredsCount);
		} else {
			service = Executors.newCachedThreadPool();
		}
		final CountDownLatch latch = new CountDownLatch(blocksCount);
		for (int i = 0; i < blocksCount; i++) {
			final int fromRow = i * blockSize;
			final int toRow = Math.min((i + 1) * blockSize, thisRows);
			service.execute(() -> {
				multiplyNative(other, dest, fromRow, toRow);
				latch.countDown();
			});
		}
		try {
			latch.await();
		} catch (InterruptedException e) { // NOSONAR
			throw new MatrixException("WTF", e);
		}
		service.shutdown();
		return dest;
	}

	@Override
	public final T multiplyNaive(Matrix other) {
		validateMultiplicand(other);
		final int thisRows = this.getRows();
		final int otherColumns = other.getColumns();
		final T dest = create(thisRows, otherColumns);
		final int thisColumns = this.getColumns();
		for (int row = 1; row <= thisRows; row++) {
			for (int col = 1; col <= otherColumns; col++) {
				int c = 0;
				for (int k = 1; k <= thisColumns; k++) {
					c += this.getInt(row, k) * other.getInt(k, col);
				}
				dest.set(row, col, c % 2);
			}
		}
		return dest;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Matrix multiply(Matrix other) {
		validateMultiplicand(other);
		if (getClass().isInstance(other)) {
			return multiplyRowsByExecutors((T) other, 16, 8);
		} else {
			return multiplyNaive(other);
		}
	}

	final void validateMultiplicand(Matrix other) {
		if (this.getColumns() != other.getRows()) {
			throw new IllegalArgumentException("Multiplicand (argument) rows count must "
					+ "equals to multiplyer (this) colum count.");
		}
	}

	public final T multiplyNativeNaive(T other) {
		final int thisRows = this.getRows();
		final T dest = create(thisRows, other.getColumns());
		multiplyNative(other, dest, 0, this.getRows());
		return dest;
	}

	static void validate(byte[][] array) {
		validate(array, false);
	}

	static void validate(byte[][] array, boolean square) {
		if (array == null) {
			throw new NullPointerException("Matrix data can't be null");
		}
		final int rows = array.length;
		if (rows <= 0) {
			throw new IllegalArgumentException(
					"Matrix data should have at least one row");
		}
		if (array[0] == null) {
			throw new NullPointerException("Matrix row can't be null");
		}
		final int columns = array[0].length;
		if (columns <= 0) {
			throw new IllegalArgumentException(
					"Matrix data should have at least one column");
		}
		if (square && rows != columns) {
			throw new IllegalArgumentException("Matrix should be square");
		}
		for (int i = 1; i < rows; i++) {
			if (array[i] == null) {
				throw new NullPointerException("Matrix row can't be null");
			}
			if (array[i].length != columns) {
				throw new IllegalArgumentException(
						"All matrix coluns shuld be equal size");
			}
		}
	}
}
