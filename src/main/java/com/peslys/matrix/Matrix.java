package com.peslys.matrix;

/**
 * The Interface Matrix represents general immutable matrix interface.
 */
public interface Matrix {

	/**
	 * Gets the count of matrix columns.
	 *
	 * @return the count of matrix columns
	 */
	int getColumns();

	/**
	 * Gets the count of matrix rows.
	 *
	 * @return the count of matrix rows
	 */
	int getRows();

	/**
	 * Gets the double value of matrix at given row and column.<br>
	 * Indexes <b>starts from 1</b.
	 *
	 * @param row the row
	 * @param col the col
	 * @return the double value
	 */
	default double getDouble(int row, int col) {
		return getInt(row, col);
	}

	/**
	 * Gets the boolean value of matrix at given row and column.<br>
	 * Indexes <b>starts from 1</b.
	 *
	 * @param row the row
	 * @param col the col
	 * @return the boolean value
	 */
	default boolean getBoolean(int row, int col) {
		return getInt(row, col) != 0;
	}

	/**
	 * Gets the int value of matrix at given row and column.<br>
	 * Indexes <b>starts from 1</b.
	 *
	 * @param row the row
	 * @param col the col
	 * @return the int value
	 */
	int getInt(int row, int col);

	/**
	 * Multiply this matrix by given (other) matrix simplest naive way.
	 *
	 * @param other the multiplicand (other)
	 * @return the matrix multiplication result (product)
	 */
	Matrix multiplyNaive(Matrix other);

	/**
	 * Multiply this matrix by given (other) optimized (parallel, native etc.) way.
	 *
	 * @param other the multiplicand (other)
	 * @return the matrix multiplication result (product)
	 */
	Matrix multiply(Matrix other);
}
