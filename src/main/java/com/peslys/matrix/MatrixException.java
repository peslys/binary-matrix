package com.peslys.matrix;


public class MatrixException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MatrixException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MatrixException(String message, Throwable cause) {
		super(message, cause);
	}

	public MatrixException(String message) {
		super(message);
	}
}
