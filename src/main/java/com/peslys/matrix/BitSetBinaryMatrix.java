package com.peslys.matrix;

/**
 * The Class BitSetBinaryMatrix implements binary matrix using bit set (bit masks) to store data.
 */
public class BitSetBinaryMatrix extends AbstractBinaryMatrix<BitSetBinaryMatrix> {

	private final int rows;
	private final int columns;
	private final byte[] data;

	private static final int BITES = Byte.SIZE;

	public BitSetBinaryMatrix(byte[][] array) {
		validate(array);
		this.rows = array.length;
		this.columns = array[0].length;
		this.data = new byte[(rows * columns / BITES) + 1];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				set(i + 1, j + 1, array[i][j]);
			}
		}
	}

	BitSetBinaryMatrix(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.data = new byte[(rows * columns / BITES) + 1];
	}

	BitSetBinaryMatrix(int size) {
		this(size, size);
	}

	@Override
	public int getRows() {
		return rows;
	}

	@Override
	public int getColumns() {
		return columns;
	}

	@Override
	public int getInt(int row, int col) {
		if (row < 1 || row > rows) {
			throw new IllegalArgumentException("row index out of bounds");
		}
		if (col < 1 || col > columns) {
			throw new IllegalArgumentException("column index out of bounds");
		}
		final int offset = (row - 1) * columns + (col - 1);
		return (this.data[offset / BITES] >> (offset % BITES)) & 1;
	}

	@Override
	void set(int row, int col, int value) {
		final int offset = (row - 1) * columns + (col - 1);
		if (value != 0) {
			this.data[offset / BITES] |= 1 << (offset % BITES);
		} else {
			this.data[offset / BITES] &= ~(1 << (offset % BITES));
		}
	}

	@Override
	final void multiplyNative(final BitSetBinaryMatrix other,
			final BitSetBinaryMatrix dest, final int fromRow, final int toRow) {
		final int otherColumns = other.columns;
		final byte[] otherData = other.data;
		final byte[] destData = dest.data;
		for (int i = fromRow; i < toRow; i++) {
			for (int j = 0; j < otherColumns; j++) {
				int c = 0;
				for (int k = 0; k < this.columns; k++) {
					final int os1 = i * this.columns + k;
					final int os2 = k * otherColumns + j;
					c += ((this.data[os1 / BITES] >> (os1 % BITES)) & 1)
							* ((otherData[os2 / BITES] >> (os2 % BITES)) & 1);
				}
				final int offset = i * otherColumns + j;
				if (c % 2 != 0) {
					destData[offset / BITES] |= 1 << (offset % BITES);
				} else {
					destData[offset / BITES] &= ~(1 << (offset % BITES));
				}
			}
		}
	}

	@Override
	final BitSetBinaryMatrix create(int rows, int columns) {
		return new BitSetBinaryMatrix(rows, columns);
	}
}
