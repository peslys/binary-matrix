package com.peslys.matrix;

/**
 * The Class ArrayBinaryMatrix implements binary matrix using two dimensions bytes array to store data.
 */
public class ArrayBinaryMatrix extends AbstractBinaryMatrix<ArrayBinaryMatrix> {

	private final byte[][] data;

	public ArrayBinaryMatrix(byte[][] array) {
		validate(array);
		final int rows = array.length;
		final int columns = array[0].length;
		this.data = new byte[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				this.data[i][j] = (byte) (array[i][j] != 0 ? 1 : 0);
			}
		}
	}

	ArrayBinaryMatrix(int rows, int columns) {
		this.data = new byte[rows][columns];
	}

	ArrayBinaryMatrix(int size) {
		this(size, size);
	}

	@Override
	public int getRows() {
		return data.length;
	}

	@Override
	public int getColumns() {
		return data.length > 0 ? data[0].length : 0;
	}

	@Override
	public int getInt(int row, int col) {
		return data[row - 1][col - 1];
	}

	@Override
	void set(int row, int col, int value) {
		data[row - 1][col - 1] = (byte) (value != 0 ? 1 : 0);
	}

	@Override
	final void multiplyNative(final ArrayBinaryMatrix other, final ArrayBinaryMatrix dest,
			final int fromRow, final int toRow) {
		final int otherColumns = other.getColumns();
		final int thisColumns = this.getColumns();
		for (int i = fromRow; i < toRow; i++) {
			for (int j = 0; j < otherColumns; j++) {
				int c = 0;
				for (int k = 0; k < thisColumns; k++) {
					c += this.data[i][k] * other.data[k][j];
				}
				dest.data[i][j] = (byte) (c % 2);
			}
		}
	}

	@Override
	final ArrayBinaryMatrix create(int rows, int columns) {
		return new ArrayBinaryMatrix(rows, columns);
	}
}
