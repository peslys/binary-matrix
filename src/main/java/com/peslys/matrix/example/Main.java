package com.peslys.matrix.example;

import java.io.PrintStream;
import java.util.Random;
import java.util.Scanner;

import com.peslys.matrix.ArrayBinaryMatrix;

public class Main {

	private static final Random RND = new Random();

	private static final PrintStream out = System.out; // NOSONAR

	public static void main(String[] args) {
		final int size = readInput();
		if (size <= 0) {
			return;
		}

		out.printf("Preaparing random data for %sx%s binary matrixes...%n", size, size);
		final byte[][] dat1 = randomData(size);
		final byte[][] dat2 = randomData(size);
		final ArrayBinaryMatrix mtx1 = new ArrayBinaryMatrix(dat1);
		final ArrayBinaryMatrix mtx2 = new ArrayBinaryMatrix(dat2);

		out.println("Multipying \"naive\"...");
		final long begin1 = System.currentTimeMillis();
		mtx1.multiplyNaive(mtx2);
		final long end1 = System.currentTimeMillis();

		out.println("Multipying by threads...");
		final long begin2 = System.currentTimeMillis();
		mtx1.multiply(mtx2);
		final long end2 = System.currentTimeMillis();

		out.printf("%nMultiplifaction of %sx%s binary matrixes tooks:%n", size, size);
		out.printf("%s for \"naive\" approach.%n", formatPeriod(begin1, end1));
		out.printf("%s for parallel (multithreaded with some optinisations) approach.%n", formatPeriod(begin2, end2));
	}

	private static byte[][] randomData(int size) {
		final byte[][] arr = new byte[size][size];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = (byte) (RND.nextBoolean() ? 1 : 0);
			}
		}
		return arr;
	}

	private static String formatPeriod(final long msBegin, final long msEnd) {
		long period = msEnd - msBegin;
		final int ms = (int) (period % 1000);
		period /= 1000;
		final int s = (int) (period % 60);
		period /= 60;
		final int m = (int) (period % 60);
		period /= 60;
		final StringBuilder sb = new StringBuilder();
		if (period != 0) {
			sb.append(period);
			sb.append("h ");
		}
		if (period != 0 || m != 0) {
			sb.append(m);
			sb.append("m ");
		}
		sb.append(s);
		sb.append('.');
		sb.append(ms);
		sb.append('s');
		return sb.toString();
	}

	private static int readInput() {
		try (Scanner scanner = new Scanner(System.in)) {
			while (true) {
				out.print("Enter positive number in range [1..10000] or q to quit: ");
				String value = scanner.nextLine();
				if ("q".equalsIgnoreCase(value)) {
					return -1;
				}
				try {
					int size = Integer.parseInt(value);
					if (size >= 1 && size <= 10000) {
						return size;
					}
				} catch (NumberFormatException ex) {
					// ignore
				}
			}
		}
	}
}
